package Byre.byre.core;

public class BouncyBall extends Sprite {

	float xvel, yvel;
	float xpoint, ypoint;
	
	public BouncyBall() {
		xvel = (float) (Math.random() * 2);
		yvel = (float) (Math.random() * 2);
	}
	
	public void doPhys() {
		xpoint += xvel;
		ypoint += yvel;
		
		if (xpoint < 0) {
			xpoint = 0;
			xvel *= -1;
		}
		if (xpoint > 248) {
			xpoint = 248;
			xvel *= -1;
		}
		if (ypoint < 0) {
			ypoint = 0;
			yvel *= -1;
		}
		if (ypoint > 248) {
			ypoint = 248;
			yvel *= -1;
		}
		
		xpos = (int) xpoint;
		ypos = (int) ypoint;
	}
	
}
